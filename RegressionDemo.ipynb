{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data Science Project - Regression Demonstration\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Project Purpose - Demo and explain (simple) linear regression\n",
    "This notebook was created for two reasons:\n",
    "1. Primarily, to outline an example of how to compute a (simple linear) regression model and apply it to some (artificial) data and,\n",
    "- Secondarily, to introduce the underlying theory of the model as supplemental information.\n",
    "\n",
    "The linear regression model equation, in its most **simple** form (i.e. two dimensional), is the common line slope equation:\n",
    "\n",
    "$$Y = mx + b \\quad\\text{or}\\quad Y = \\beta_1 X + \\beta_0 $$\n",
    "\n",
    "The regression model equation in a more generalized form has $n$ dimensions. When a regression model has more than one independent variable, it is a *multiple* linear regression model, and has the equation:\n",
    "\n",
    "$$Y = \\beta_0 + \\beta_1 X_1 + \\beta_2 X_2 + ... + \\beta_n X_n + \\varepsilon \\text{,} \\quad\\text{i.e.} \\quad Y = \\beta^T X + \\varepsilon \\quad\\text{or} \\quad h(X_i) = \\sum_{i=1}^n\\beta_i X_i + \\varepsilon$$\n",
    "\n",
    "Where\n",
    "\n",
    "Symbol  |  | Name | | Additional Names\n",
    "-: | :-: | :- | :- | :-\n",
    "$Y$ | is the | **dependent variable** | or | *outcomes*, *output*, *responses*, *predicted value*\n",
    "$y$ | is a | **dependent variable observation** | or | observation\n",
    "$X_{1-n}$ | are the | **independent variables** | or | *predictors*, *covariates*, *features*\n",
    "$x_{1-n}$ | are the | **independent variable observations** |  | \n",
    "$\\beta_{0-n}$ or $\\theta_{0-n}$ | are the | **true model parameters*** | or |  *regression coefficients*, *weights*\n",
    "$\\beta_0$ | is the | **model bias** | or | *intercept*\n",
    "$h()$ | is the | **hypothesis function** | |\n",
    "$\\hat\\beta_{1-n}$ | are the | **estimated model parameters** | or | *model estimates*\n",
    "$\\varepsilon$ | is the | **sample error** | or | *sample deviation*\n",
    "\n",
    ">**(Sometimes $\\theta$ is used instead of $\\beta$. $\\theta$ is often used for general model parameters whereas $\\beta$ is used for regression model parameters)*\n",
    "\n",
    "A linear regression model is valid under four assumptions:\n",
    "\n",
    "| |Assumption|Explanation | Mathematical representation|\n",
    "-|-|-|-\n",
    "1|**Linearity**| There is a linear relationship between the independent variable(s) ($x$) and the dependent variable ($Y$)| $Y = \\beta^T X + \\varepsilon $\n",
    "2|**Normality**| The error residuals ($\\varepsilon$) follow a normal distribution|$$\\varepsilon = f(\\mu,\\sigma) =\\dfrac{e^{(\\dfrac{-(X-\\mu)^2}{2\\sigma^2})}}{\\sigma\\sqrt{2\\pi}}$$\n",
    "3|**Homoscedasticity**| The variance of the error residuals is the same for all values of the independent variable(s) ($X$)| $\\sigma_{X_1}^2 = \\sigma_{X_2}^2 = ... = \\sigma_{X_n}^2$\n",
    "4|**Independence**|Each observation $(y_i,x_i)$ is independent of all other observations $(y_{j\\neq i},x_{j\\neq i})$| $y_i = f(x_i\\require{cancel}\\cancel{,x_j})$\n",
    "\n",
    "[//]: # (\n",
    "This is an unrendered comment!\n",
    "To do strike through: \n",
    "$\\require{cancel} \\cancel{horizontalstrike}$\n",
    "or \n",
    "$\\require{enclose} \\enclose{horizontalstrike}{test}$\n",
    ")\n",
    "\n",
    "To **fit a regression model** to some data is to estimate the **optimal model parameters** ($\\hat\\beta_{0-n}$) based on some sort of evaluation criteria involving said data.\n",
    "\n",
    "An objective evaluation criteria is a cost function, $J(\\beta_{0-n})$, and the optimal model parameters are the parameters which minimize the cost function.\n",
    "\n",
    ">*(The cost function uses a $J$ because it is based off the **Jacobian** matrix, invented by **Jacobi**.)*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Data Source - Generate 2D data set\n",
    "For this demonstration, the data needed to perform a simple linear regression will be generated directly. **Simple** linear regression means only one **independent variable**, therefore we need a set of $(x,y)$ pairs to represent our 2D data.\n",
    "\n",
    "Noise is included to add realism to our data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 126,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Numpy will be used to generate the data\n",
    "import numpy as np\n",
    "\n",
    "#Matplotlib will be used to visualize the data\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "#Set our true model parameters\n",
    "bias = 2\n",
    "theta_1 = 5\n",
    "\n",
    "#Generate the independent variables\n",
    "np.random.seed(0)\n",
    "num_data_points = 100\n",
    "x = np.random.rand(num_data_points,1)\n",
    "\n",
    "#Generate random noise with a normal distribution \n",
    "noise = np.random.normal(0,1.5,(num_data_points,1))\n",
    "\n",
    "#Compute the dependent variable values and add noise\n",
    "y_noiseless = bias + theta_1*x\n",
    "y = y_noiseless + noise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Data Structure and Quality - Visualize the data with a scatter plot\n",
    "Since we generated our data, we don't need to clean it, just visualize it. For a real-world data science project, cleaning data is very important.\n",
    "\n",
    "> Notice how the noisy data surrounds the noiseless data. The noise we added varied between $-0.5$ and $+0.5$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 127,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAX4AAAEGCAYAAABiq/5QAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+j8jraAAAgAElEQVR4nO3de3SV1Z038O8PQoswcpFbGQGhU8dyC7dQEhTL5R0TC1XfvlPHUWSWjk079Up1VeUtxkxf81pxvZnSdurKEi9FWp2lrqm3ouNABpBADVa0gKMdKxAVDIyJNUqb5PzeP55zkpOTc5Ln5Dy3/ezvZ62sk8uTnP2cc/I7+/ntvX9bVBVERGSPQWE3gIiIgsXAT0RkGQZ+IiLLMPATEVmGgZ+IyDJFYTfAjbFjx+rUqVPDbgYRkVH27dt3QlXHZX7ft8AvIg8AWAXgA1WdlfzeGQAeAzAVwDsALlXVD/v7W1OnTkVjY6NfTSUiiiUROZzt+36meh4CUJHxvdsA/Luqng3g35NfExFRgHwL/Kq6A8B/Z3z7YgAPJz9/GMAlft0/ERFlF/Tg7gRVfR8Akrfjcx0oIpUi0igijc3NzYE1kIgo7iI7uKuqdQDqAKCkpKRXXYn29nY0NTXh1KlTgbeN8jd06FBMmjQJQ4YMCbspRNYLOvAfF5GJqvq+iEwE8MFA/1BTUxNOP/10TJ06FSLiYRPJa6qKkydPoqmpCdOmTQu7OUTWCzrV8xSAv0t+/ncAfjnQP3Tq1CmMGTOGQd8AIoIxY8bw6owoInwL/CLyCwANAM4RkSYR+XsAdwP4KxF5C8BfJb8u5D4KbygFgs8VkUuJBHD8OOBj5WTfUj2q+rc5frTCr/skIjJaIgEsWwbs3g0sXgxs3w4M8r5/zpINBRAR3HzzzV1f33vvvbjzzjv7/J377rsPP/vZz/K+r/r6eqxatSrv38vXO++8g1mzZvV7zM9//nPf20JkneZmJ+h3dDi3Ps1oZOAvwGc/+1k8+eSTOHHihOvf+da3voU1a9b42Cr/MfAT+WT8eKenX1Tk3I7POeO9IAz8BSgqKkJlZSVqa2t7/ezw4cNYsWIFiouLsWLFChw5cgQAcOedd+Lee+8FAGzcuBEzZsxAcXExLrvsMgBAW1sbrr76aixcuBDz5s3DL3/Ze/w71zEHDhzAl770JcydOxfFxcV466230NbWhpUrV2LOnDmYNWsWHnvssV5/b9++fZgzZw7Kysrwk5/8pOv777zzDpYsWYL58+dj/vz52L17NwDgtttuw86dOzF37lzU1tbmPI6I8iTipHeamoD6eudrP6hq5D8WLFigmQ4ePNjre0EbPny4tra26llnnaUtLS26YcMGraqqUlXVVatW6UMPPaSqqps2bdKLL75YVVWrqqp0w4YNqqo6ceJEPXXqlKqqfvjhh6qqevvtt+vmzZu7vnf22Wfrxx9/rNu3b9eVK1f2ecx1112njzzyiKqq/vGPf9RPPvlEH3/8cb3mmmu62tzS0tLrPGbPnq319fWqqnrLLbfozJkzVVW1ra1NP/30U1VVffPNNzX1PKS3pa/jMkXhOSOyCYBGzRJTrerxNzQAFRXOrVdGjBiBNWvWYOPGjRn31YDLL78cAHDllVdi165dvX63uLgYV1xxBR555BEUFTnj7C+88ALuvvtuzJ07F0uXLsWpU6e6rhZSch1TVlaGmpoa/OAHP8Dhw4dx2mmnYfbs2XjxxRdx6623YufOnRg5cmSPv9Xa2oqWlhZ8+ctf7mprSnt7O77xjW9g9uzZ+PrXv46DBw9mfQzcHkdE0RDZlbt+qK4Gnn/e+XzrVu/+7k033YT58+fjqquuynlMtumMzz77LHbs2IGnnnoK3//+93HgwAGoKp544gmcc845PY49fvx41+e5jpk+fToWLVqEZ599FuXl5bj//vuxfPly7Nu3D8899xxuv/12XHDBBbjjjjt6/K1cUy1ra2sxYcIE7N+/H4lEAkOHDi3oOCKKBqt6/FVVQHm5c+ulM844A5deeik2bdrU9b3Fixfj0UcfBQBs2bIF5513Xo/fSSQSOHr0KJYtW4Z77rkHLS0t+Pjjj1FeXo4f/ehH0OQc3t/85je97i/XMW+//TY+//nP44YbbsBFF12E1157De+99x6GDRuG1atX45ZbbsErr7zS42+NGjUKI0eO7Loi2bJlS9fPWltbMXHiRAwaNAibN29GZ2cnAOD000/HH/7wh36PI6Josirwl5U5Pf2yMu//9s0339xjds/GjRvx4IMPori4GJs3b8YPf/jDHsd3dnZi9erVmD17NubNm4e1a9di1KhRWL9+Pdrb21FcXIxZs2Zh/fr1ve4r1zGPPfYYZs2ahblz5+KNN97AmjVr8Prrr3cN+N5111343ve+1+vvPfjgg7j22mtRVlaG0047rev73/72t/Hwww+jtLQUb775JoYPHw7ASVEVFRVhzpw5qK2tzXkcEUWTpHqNUVZSUqKZG7EcOnQI06dPD6lFNBB8zoiCJSL7VLUk8/tW9fiJiIiBn4jIOgz8RESWYeAnIrIMAz8RkWUY+ImILMPAX4AgyzL3ZerUqf1WCK2pqfH0PonIXAz8BTCpLDMDP1kngJ2sTMXAXwCvyzInEgmcffbZaE5uvpBIJPCFL3yh1xvLyZMnccEFF2DevHn45je/ifRFeJdccgkWLFiAmTNnoq6uDoBTRvnTTz/F3LlzccUVV+Q8jig2UjtZTZoELF3qfE3dspXsjNqHZ2WZOztVjx1TTSTy/90s/CjLfOedd2ptba2qqj7//PP6ta99rdf9Xn/99VpdXa2qqs8884wC0ObmZlVVPXnypKqqfvLJJzpz5kw9ceJEV1vT5TrOTyzLTIE5dky1qEgVcG6PHQu7RaGA9WWZfeoBeF2W+eqrr+4aA3jggQeyVvzcsWMHVq9eDQBYuXIlRo8e3fWzjRs3Ys6cOSgtLcXRo0fx1ltvZW232+OIjBTQTlamsifw+7iX5U033YRNmzahra0t5zG5yjJfe+212LdvHxYsWICOjg5MnjwZEyZMwLZt27B3715ceOGFrv9efX09XnzxRTQ0NGD//v2YN28eTp06NeDjiIwV1E5WhrIn8PvYA/CyLDMAXHPNNVi9ejUuvfRSDB48uNf9nX/++V3lk3/1q1/hww8/BOCURx49ejSGDRuGN954A3v27On6nSFDhqC9vb3f44hiY9AgYMIEBv0s7An8PvcAvCrLDAAXXXQRPv7445wbu1RVVWHHjh2YP38+XnjhBUyZMgUAUFFRgY6ODhQXF2P9+vUoLS3t+p3Kysqu1FJfxxFR/LEscwQ1NjZi7dq12LlzZ9hN8VScnzOiKMpVltmqrRdNcPfdd+OnP/1pj52wiIi8ZE+qxxC33XYbDh8+3GtMgIjIK0YHfhPSVOTgc0UUHcYG/qFDh+LkyZMMKAZQVZw8eRJDhw4NuylEBINz/JMmTUJTU1NXeQOKtqFDh2LSpElhN4OIYHDgHzJkCKZNmxZ2M4iIjGNsqoeIiAaGgZ/IJixVTGDgJ7JHZqHC99/nG4DXDHljDSXwi8haETkgIr8VkV+ICKd7EPktvVDhzp3A5MmsVe8lg/YACDzwi8iZAG4AUKKqswAMBnBZ0O0gsk56oUJVoLPT80q1VvOxArDXwkr1FAE4TUSKAAwD8F5I7SCyR6pQ4dGjwJIlrFXvNYP2AAh8Oqeqvisi9wI4AuBTAC+o6guZx4lIJYBKAF3VJ4moQIMGAZ/7nFOhtrnZCU4sW+yN1BurAY9rGKme0QAuBjANwJ8DGC4iqzOPU9U6VS1R1ZJx48YF3UyieGOten8Y8riGker5HwB+r6rNqtoO4EkAi0NoBxGRlcII/EcAlIrIMHH2D1wB4FAI7SAislLggV9V9wJ4HMArAF5PtqEu6HYQRZoh88HJTKHM6lHVKlX9oqrOUtUrVfWPYbSDKJIMmg9OLkXsjZwrd4mixqD54ORCBN/IGfiJosag+eDkQgTfyBn4ifwy0Mv71HzwpiZnvn3EpwZSPyL4Rs7AT+SHQi/vDZkPTi5E8I2cgZ/IDxG8vKcQReyNnIGfyA8RvLw3TsRmwsQJAz+RHyJ4eW+UCM6EiRMGfiK/ROzy3ihMlfmKgZ+IoiWRcNI7ZWVMlfmEgZ+IoiOV4pk82blSOnKEqTIfMPATUXRkpngGDTIv6BswKM3AT0TRYfpsKEMGpQPfgYuIKCeDdrHKKtug9IQJYbeqF/b4KZ4MuNymHMKaDeXFa8aQKxYGfoofry63+eaRH5MfL69eM4as32Dgp/jxYg64IbnayDD98fJy3YCHVywNDUBFhXPrJQZ+ih8vLre5gCg/pj9eEUnR1NUBo0YBM2c6wb66Gnj+eefWSwz8FD9eXG5HJBBEgpsUjumPV0gpmswe/f++PYHPth7HwYOK6mqgqgooL3duvSRqQD6upKREGxsbw24G2SaRMHd2iVdSKZzdu52Avn27k8rIdaztj1eeKiqcHn15ObD1uQTe++IyjHtrN14dthgdL2xH2bmF9c1FZJ+qlmR+nz1+olyiUGsn7AHTfFI4UXi8IqquDhg71rlN16NH39yMP//9bgxBBxb+aTfKvuBfuoyBnyiqojBganoKJyLWrQNOnnRu05WVAVu3OrdBPtYM/ERRFYUBU0OmJ0ZFQwNQWup8pM/EqakBxoxxbnMK8LFmjj9OmGONF1Wnp5/KrzPwRl4qZw8k8/Zbw21Prhw/SzbERT6DcBSsgb4h51u+gG/8oauqAlpauj+PKkaGuIhCWoB6C2rT9SiMBxDKyoA9e5yPsrKwW5MbA39ccBAumoJ6Q+Ybv6/8WkEbFgb+uOAgXG5hTokM6g2Zb/y+8msFbVgY+OOE86i7pYJ9Z2e4KZCg3pDT72fbNuCDD8wslhaivnr1fq2gDQsDP8VPer57yRLgpZfCTYEE9YY8aBAwbhywfDlz/QPQV6++x3z7GGDgp/hJz3e//DKwcKE9KRDm+gesz1592CuoPcbAT/GTme/etcuesY98c/0xC2jZuB2Yzdmrj+GMKc7jp76ZODc82/z3CG5/54t85v5Hee1Hga+7hgZg7drur/fudW4HtKDKkO0U8xHKsywio0TkcRF5Q0QOiUhMMmcxY3JPx+aBbrfnHtW0UAGvu1Tvfu1aJ9inAn5BA7MxnDEVSskGEXkYwE5VvV9EPgNgmKq25DqeJRtCcvy488/X0eG86JuajO/pUJqoloQo4HWXKpmwaFH392prPRiUNfHKFxEqyywiIwCcD2ATAKjqn/oK+hSiGPZ0AFiR13alkKmmfj6G/bzu3Ey7rK31eAVtzK4gA+/xi8hcAHUADgKYA2AfgBtVtS3juEoAlQAwZcqUBYcPHw60nZRkaE8npyjntU0RxGOY5XWX2oqwpcVJ4UShCJrvCvz/i0yPH86A8nwAP1XVeQDaANyWeZCq1qlqiaqWjBs3Lug2UkrMejqRzWubJIDHsGHvIJRePAGlZdLVs0/NswfitZgqJx/H2MII/E0AmlQ1OeyCx+G8ERD5L67pK69EYH/dhgbgq1/tHpxNLahKT+PEaTFVTj6+wQYe+FX1GICjInJO8lsr4KR9iPzHmka5ue1h+vwYVlc7u1WNHOkM0qZ69nFbPZtV+huvj2+wYc3jvx7AluSMnrcBXBVSO8hGqfRVHHg5BpPPfHUfH8NUoK+qinmQz5Rt7CSf/RjyEMqolqq+mszfF6vqJar6YRjtIDKa1zlgH3uY+ZQ1tqJnn022N16fxtg4nYHIVF7ngN2kcAY4jTNuZY3zFoGxk3QM/ESm8iNQ9NXDLOAKo2p9An+z9Diq7rBw7URExk7SMfCTWbj4qptXgcLtY9rPFUZdHTB2rHOb+ffL1i3Do7smoez2pWaV/vBCPldmAU2fZuAnc5hcO8gvhQaKfB7Tfq4w1q1zZuOsW5fxe2GsnYhSByGCU4gZ+MMSpRemKbj4ynv5PKYiaKjZjsvOa0Ld5fWouFB6DNbW1ABjxji3PQQd+KLWQYjiFGJVjfzHggULNFY6O1XPP1+1qMi57ewMu0VmSCR6Pm6JRNgtMp+Lx3T3btXy8u5bQHXMGOe2vNzl/XR2qh47Vthz5vZvHDvmnA/g3B47NvD7NByARs0SU9njDwN7rgMTxZ6T6bI8pplTL9Nn5KRWz9bU5Fk2IUIpKeJGLOFIvTBTCzX4wswtc4FSnBZfRUXaY1pXB1x3HdDe7vxo69beC6pShdEqKwNsYz6Ly/LZjMZS7PGHgT1Xd6KWq42hhgagtNT5aGhwBmbb24EhQyJWKiHfXrwpxQVDGutj4A+LKS/MMDEl5ptsO1VVV3cP0P74xxFbORvHzlKIHZtQduDKF3fgslRUd4jym497IGTWtM+6U1Xc9mCIqgB2uItSPX4yXVCXp3Hs5fXH515gZk37XjtVMb0WnBAHoRn4KT9BBwbbUmIFpLdS6Ztbb82xghYuatozvRacEDs2TPVQfvK9PI1i2iCKbUrJM73V0ODk6VP27nUGZtvbnVz9iRP+3j9FG1M95I18Lk+jmDZw26awVlbn2Qusru4enAWc3vzatTlW0Ppw/2Smfnv8InIdgC0aYs189vgjxm2POYDBq7y5aVNEN2RPDcymb1CS3uPvGpwlSiqkx/85AC+LyL+ISIUIuwDWc5t3j+IKSjdtilCeO30Vbbaa9mVlGYOzAOtAUb/6Xbmrqt8TkfUALoCzReKPReRfAGxS1f/yu4FksCiuoHTTppBXVtfVAd/5jvP5WWcBB5M7UqevoM0polcrFC2uB3dFZA6cwF8BYDuAUgD/pqrf9a95DqZ6KHAhDgCPHeuUNwaAESOcnrzr/WejmF4LUpQH7kMw4FSPiNwgIvsA3APgJQCzVfUfACwA8L88bylRFPg8jTSVwqmr670XbU0NMHy487FhQ57lEqKYXgtKFCcTRJSbwd1/hJPWOZzlZ9NV9ZBfjUthj5/ipqLCydePGeP07svLu4ufFczWXq/tVztZDLjHr6p3ZAv6yZ/5HvQpZIUMFHKQEQB6lTkGCihv7EZYi97Cfr5tvtrJExdwUW6FDBRykLFrquUbbwCtrR736qMmKs+3rVc7OeTq8TPwU26FXDrzsrsrnQM4KZ2nn47xPHs+35HElbuUv0IunXnZjaoqp/rlokUxD/oAn2/DsMdPfSvk0jms36Vw8DmLHPb4aWAKGSgc6O92dADnncdpeaaxrZKqwRj4+xP2TAXbJBLAkiXOyGjIJROyzcaJPb7ercDA3xcuCAleczPw8svdXy9cGHi+OLUP7YUX9q6NE2t8vVuDgb8vESrWZY3x44FzzwUGD3ai765dgaUOMvehbW11ZuN4Osc+yuL6eudVTC8M/H0ZyEwFvsgKkyqi9u67TvDxYS54rhRO+raE1szGSRfHmTm8ismKs3r6k89MhagsYqE+zZzpVLycMQM4cKD7+9nq3VsnbjNzLF9fELlZPSIyWER+IyLPhNUGV/KZqRDXS2XDZfbwm5p63qaUleVZEC2O4jYzJ45XMR4Iszt6I4B41frhiywy6uq6NxzP3MBkwwYnd79hQ7htpABwK8msQkn1iMgkAA8DuAvAd1R1VV/HG7WAK26XyoZK1bRPlUqwPoUTJ/wfcy1qqZ5/AvBdADlHWkSkUkQaRaSx2aSUidtL5UIHgTmI3Keamu4Nx5nCiREO1noi8MAvIqsAfKCq+/o6TlXrVLVEVUvGjRsXUOsCUuiL1+IXv9tFVZWVwIkTzi3FCMfRPBFGj/9cABeJyDsAHgWwXEQeCaEd4cnnxZutZ2/xiz/bhuNkEY6jeSLwwK+qt6vqJFWdCuAyANtUdXXQ7QiV2xdvrp69BS/+XFsTpjYwsWZRlRs2pf04WOuJorAbYAwvB5RSL97+/l62nn1q/MDN7xuqoQH46ledwdnGxu6Nx1N5+thuZtKfbK9BG9eOpMbRaMBCfYWoan1/M3oiwY+cuptB4L569nGbb52murp7Ro4vWxOaILMXn+s1aHHajwYu5l0Dj/j5z9XXZbqll7WpdM7TTzuDs9bNyMkW5HO9Bi1I+5H3GPjd8Oufy82VRIx79rlYP/0yW5DP9Rq0tHNAhWHgd8Ovf66YX6Y3NDh1cUaNcgZpyaVsQb6v16CFnQMqDAO/W339cw10VkXML9Orq51iaK2twLp1YbfGILmCPAM8eYSBv1CFDPzG5DI916KqqiqnAubIkc4gLeWBQZ58xOmchco15dItg6empcoYt7Q4G5cAPadalpX1LHtMRNHAHn+hYp6uySZ9p6rUxiVWTrkkMhQDf6Fikq7JJb28cUr6TlXl5UBtreWzcIgMwx24qE/p5Y1PnHC+x52qiMwQtbLM9jCgjkr64GzmQG16eeMU6+fZExmOg7t+MqSOSnrqBuj+fOtWZ+UsSxsTxQsDv58KnfETkNSgbPrgLAdqPcLdoiiCGPj9lJrxk+rxR3TGT2bFS2urX3rNkCs+sg9fhX4KccZPQwNQWup89LdbFSV5PR7jpiRHtvs0YFyIzMbA77cAV2CmD8xWVzuLqvbuzXO3KluDjh+lt/tb45HtPi3eVpOCw1RPviKcs00fpK2qclbUpj53xebUhB/jMf1tmJPrisCAcSEymyX/1R6JUG8sW32c9G0Jy8qAPXucD9fTLv2oFmrKFYRfK7D7uuLLdp8WrgSn4HEBVz6OH3eCfkeH84/Z1BRab6yiwundl5d7OBir6ryhpXr8hY5LmHYFEcbVXK7tFCN6VUlm4QIuLwTcG+trgNaXTce9How2bb+BMCpiZrtPVuYkn7HHn68Ae2OpXj3gcc8+KF5fQRBRXtjj94rHvbFUrr6uLnvOftEi58PIBVUxL2BHZCrO6glJXZ2zK9WECc4uVY2NTjE0oLtnnxqgNZrB+w0QxRV7/H7IMpMlM1+/bp0T6N9910nj1NSwpj0RBYOB32sZUz4bXkqgogK45pqeC6pSVS/vuae7GBorXgbElCmmRD7h4K7Xjh9H4sxJGNTZgcTgIly+pAmP1U/AiBHARx8BI0YwwIfKtCmmRAXg4G4A6uqAUX85Hi/pYrSjCL8dsRg33jUe5eXAhg3dM3MCC/rs2fY20Po5XuNzQyFi4PdAambOd78LtH4k+HJiO4pHN6HtmXqULZZwUjkRWmUcKQOpn+M1PjcUMgb+PGUrlZCqkXPmmcDIkcD0GYPwwLMTULY4xOmLpi2eCkp/U0yDeNz43FDIOJ3TpVTFy5YWZ4AW6J52mb6RSWRy94bsBRCKvqaYBvG48bmhkHFwtx+ZAX/RImDUqIgF+VxY82Vggnjc+NxQAHIN7rLH349UGmfRop6VL43AxVMDE8TjxueGQmRfjj/HbIpsuXuguxhabS2nYRJRPNgV+PuYTZHq2WfuVpXaj9b3gO/19D5OFySiHAIP/CIyWUS2i8ghETkgIjf6dmeZwa+5GZ27nNkUnbt6zqbwpcxxPu30cnofpwsSUR/C6PF3ALhZVacDKAVwrYjM8PxeEgm0LliGjomT0Dp/qRP8xncvrnpJe86mCKxnn43X0/s4XZCI+hB44FfV91X1leTnfwBwCMCZnt9RczOG79+NIu3A8P3J4CeC527ZjmlFTXj2lvpgZ1P0lXrxeoMXbt9HRH0IdTqniEwFsAPALFX9KONnlQAqAWDKlCkLDh8+nN8fV0Xr/KUYvn832uYsxshX6sObNuemPozX0/s4JZHIermmc4YW+EXkzwD8B4C7VPXJvo4d8Dz+qASmCO3V6xkWOyOKvEgVaRORIQCeALClv6BfkKD3Ls2Vzolj6oXjCETGCmNWjwDYBOCQqv6/oO/fN33NpInjFoSZb2aqnDpKZIgwevznArgSwHIReTX58ZXA7t2v+e399YCDvvrwW+rN7MgR57GcPJlTR4kMEcasnl2qKqparKpzkx/PBXLnfs5vj2M6pz+DBjkfDQ1M+RAZxK7ROD/z0iancwq5CrLxDY/IcHYFfr+DlInpnEKvgkx+wyOylF3VOVNBKgpTPKMi21VQvlNNWWmSyCh29fgBM3vlfmKqhsg6dvX4qTdeBRFZh4GfmKohsox9qR4iIssx8BMRWYaBP4U7VhGRJRj4Ae5YRURWYeAHWGmSiKxiX+DPltLhXHYisohdgT9XSodlB4jIInYF/r5SOkGs6OUAMhFFgF2BP8yUDgeQiSgi7Fq5G2Z5Ai+KoRERecCuHj8QXpG2sWOBhQs5gExEobMv8IchkQCWLwd+/WugpATYto0DyEQUGgb+IKTSPJ2dQGMjcOJE2C0iIosx8AeB6wSIKELsGtwNC2veE1GEMPAHhTXviSgimOqJMi74IiIfMPBHFRd8EZFPGPijihVDicgnDPxRxZlAROQTDu5GFWcCEZFPGPijjDOBiMgHTPUQEVmGgZ+IyDIM/F7ivHsiMgADv1c4756IDBFK4BeRChH5TxH5nYjcFkYbPMd590RkiMADv4gMBvATABcCmAHgb0VkRtDt8Bzn3RORIcKYzvklAL9T1bcBQEQeBXAxgIMhtMU7nHdPRIYII9VzJoCjaV83Jb/Xg4hUikijiDQ2m5I2CWtbRyKiPIQR+LNFxV7TYFS1TlVLVLVk3LhxATSLiMgOYQT+JgCT076eBOC9ENpBRGSlMAL/ywDOFpFpIvIZAJcBeCqEdhARWSnwwV1V7RCR6wA8D2AwgAdU9UDQ7SAislUoRdpU9TkAz4Vx30REtuPKXSIiy4gaUFdGRJoBHB7Ar44FcMLj5pjAxvO28ZwBnrdt8j3vs1S117RIIwL/QIlIo6qWhN2OoNl43jaeM8DzDrsdQfPqvJnqISKyDAM/EZFl4h7468JuQEhsPG8bzxngedvGk/OOdY6fiIh6i3uPn4iIMjDwExFZxvjA399uXuLYmPz5ayIyP4x2es3FeV+RPN/XRGS3iMwJo51ec7t7m4gsFJFOEfnrINvnFzfnLSJLReRVETkgIv8RdBv94OJ1PlJEnhaR/cnzviqMdnpJRB4QkQ9E5Lc5fl54TFNVYz/g1Pr5LwCfB/AZAPsBzMg45isAfgWnHHQpgL1htzug814MYHTy8wttOe+047bBKQDrEp0AAANFSURBVAvy12G3O6DnexSczYymJL8eH3a7AzrvdQB+kPx8HID/BvCZsNte4HmfD2A+gN/m+HnBMc30Hn/Xbl6q+icAqd280l0M4Gfq2ANglIhMDLqhHuv3vFV1t6p+mPxyD5zy16Zz83wDwPUAngDwQZCN85Gb874cwJOqegQAVDUO5+7mvBXA6SIiAP4MTuDvCLaZ3lLVHXDOI5eCY5rpgd/Nbl6udvwyTL7n9Pdwegim6/e8ReRMAP8TwH0Btstvbp7vvwQwWkTqRWSfiKwJrHX+cXPePwYwHc6eHq8DuFFVE8E0LzQFx7RQqnN6yM1uXq52/DKM63MSkWVwAv95vrYoGG7O+58A3KqqnRKfLTDdnHcRgAUAVgA4DUCDiOxR1Tf9bpyP3Jx3OYBXASwH8BcA/k1EdqrqR343LkQFxzTTA7+b3bziuOOXq3MSkWIA9wO4UFVPBtQ2P7k57xIAjyaD/lgAXxGRDlX912Ca6Au3r/MTqtoGoE1EdgCYA8DkwO/mvK8CcLc6ye/ficjvAXwRwK+DaWIoCo5ppqd63Ozm9RSANcmR8FIArar6ftAN9Vi/5y0iUwA8CeBKw3t96fo9b1WdpqpTVXUqgMcBfNvwoA+4e53/EsASESkSkWEAFgE4FHA7vebmvI/AucqBiEwAcA6AtwNtZfAKjmlG9/g1x25eIvKt5M/vgzOz4ysAfgfgEzg9BKO5PO87AIwB8M/J3m+HGl7N0OV5x46b81bVQyKyFcBrABIA7lfVrNMBTeHy+f4+gIdE5HU4KZBbVdXocs0i8gsASwGMFZEmAFUAhgDexTSWbCAisozpqR4iIsoTAz8RkWUY+ImILMPAT0RkGQZ+IiLLMPATEVmGgZ+IyDIM/EQDkKz3/5qIDBWR4cla8LPCbheRG1zARTRAIvJ/AAyFUxStSVX/b8hNInKFgZ9ogJL1Y14GcArAYlXtDLlJRK4w1UM0cGfA2fzjdDg9fyIjsMdPNEAi8hScXaGmAZioqteF3CQiV4yuzkkUluQOVx2q+nMRGQxgt4gsV9VtYbeNqD/s8RMRWYY5fiIiyzDwExFZhoGfiMgyDPxERJZh4CcisgwDPxGRZRj4iYgs8/8BvgUHJMO6cDsAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "# plot the data\n",
    "fig = plt.figure()\n",
    "fig1 = fig.add_subplot(111)\n",
    "\n",
    "fig1.scatter(x,y_noiseless,s=2,c='b',label='Noiseless data')\n",
    "fig1.scatter(x,y,s=5,c='r',label='Noisy data')\n",
    "plt.legend(loc='upper left')\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('y')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Data  Model - Compute regression coefficients\n",
    "A linear regression model can be fit using different techniques, including:\n",
    "1. Calculus and algebra.\n",
    "- Gradient descent.\n",
    "\n",
    "Both techniques aim to minimize a cost function, $J(\\beta_{0-n})$, which the model creator must decide. \n",
    "\n",
    "- Technique 1 applies calculus and algebra to determine a formula or equation wich solves for the minimum values of the cost function, i.e. $Min\\,J(\\beta_{0-n})$. \n",
    "\n",
    "- Technique 2 uses iterative computational (numerical) technique(s) to approach the minimum value of the cost function.\n",
    "\n",
    "We will explore technique 1, since not all problems are simple enough to be solved analytically like this.\n",
    "\n",
    "*Simple linear regression* uses **squared vertical deviations** for the cost function. Deviation is is the difference between a sample's value and its expected value.\n",
    "> I.e.  The expected value is the estimated mean value of a dependent variable from a set of $n$ samples sampled with the same value for the independent variables.\n",
    "\n",
    "We compute our expected value for sample $i$ using our regression model equation :\n",
    "$$\\text{Exepected value}\\,y_i=\\beta_0+\\beta_1x_i = h(x_i)$$\n",
    "\n",
    "This makes our deviation ( $ \\varepsilon_i $ ) :\n",
    "$$\\varepsilon_i = h(x_i)-y_i = \\beta_0+\\beta_1x_i - y_i$$\n",
    "\n",
    "> Deviation is sometimes also called *residual error* or just *error*, so squared vertical deviation is also called *squared error.*\n",
    "\n",
    "Therefore, our **squared vertical deviations** are :\n",
    "$$\\varepsilon_i^2 = (h(x_i)-y_i)^2$$\n",
    "\n",
    "and our cost function is the sum of all the errors in our sample set :\n",
    "\n",
    "$$J(\\beta_0,\\beta_1) = \\sum_{i=1}^n \\varepsilon_i^2 = \\sum_{i=1}^n(h(x_i)-y_i)^2$$\n",
    "> This is called the *Least Squares Technique,* and is the technique used in *Simple Linear Regression*\n",
    "\n",
    "There are other error function techniques, including *absolute error* and *total least squares*. We won't explore them now. \n",
    "\n",
    "To compute our optimal model parameters we use calculus to find the minimum of our cost function. \n",
    "\n",
    "**The optimal model parameters are the solution to the set of equations (equated to zero, for the minimum) produced from the partial derivatives of the cost function with respect to each model parameter.**\n",
    " $\\dfrac{\\partial J}{\\partial \\beta_0} = \\sum_{i=1}^n2(\\beta_0+\\beta_1x_i - y_i) = 0 \\tag{1.0}$\n",
    "\n",
    "\n",
    " $\\dfrac{\\partial J}{\\partial \\beta_1} = \\sum_{i=1}^n2(\\beta_0+\\beta_1x_i - y_i)x_i = 0 \\tag{2.0}$\n",
    "\n",
    "Here are the steps solving the equations:\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "\\tag{1.1}\n",
    "\\sum_{i=1}^n2(\\beta_0+\\beta_1x_i - y_i) &&\\:&& = 0 \n",
    "\\to && \n",
    "\\sum_{i=1}^n \\beta_0 + \\sum_{i=1}^n \\beta_1x_i = \\sum_{i=1}^n y_i\n",
    "\\to &&\n",
    "n\\beta_0 + \\sum_{i=1}^n \\beta_1x_i && = &&\\sum_{i=1}^n y_i\n",
    "\\\\\n",
    "\\tag{2.1}\n",
    "\\sum_{i=1}^n2(\\beta_0+\\beta_1x_i - y_i) && x_i && = 0\n",
    "\\to && &&\n",
    "\\sum_{i=1}^n \\beta_0x_i + \\sum_{i=1}^n \\beta_1x_i^2 && = &&\\sum_{i=1}^n y_ix_i\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "The equation for the mean (either $\\bar x$ or $\\bar y$) is :\n",
    "\n",
    "$$\n",
    "\\bar z = \\dfrac{1}{n} \\sum_{i=1}^n z_i \\quad\\therefore\\quad \\sum_{i=1}^n z_i = n \\bar z\n",
    "$$\n",
    "\n",
    "So, substituting that in yields:\n",
    "$$\n",
    "n\\beta_0 + n\\beta_1\\bar x = n\\bar y\n",
    "\\quad\\to\\quad\n",
    "\\beta_0 = \\bar y - \\beta_1\\bar x \\tag{1.2}\n",
    "$$\n",
    "and\n",
    "$$\n",
    "\\beta_0n\\bar x + \\sum_{i=1}^n \\beta_1x_i^2 = \\sum_{i=1}^n y_ix_i \\quad \\text{sub in $\\beta_0$}\\quad\\to\\quad\n",
    "(\\bar y - \\beta_1\\bar x )n\\bar x + \\sum_{i=1}^n \\beta_1x_i^2 = \\sum_{i=1}^n y_ix_i \\tag{2.2}\n",
    "$$\n",
    "And isolating $\\beta_1$ yields:\n",
    "$$\n",
    "\\begin{align}\n",
    "\\tag{2.3}\n",
    "n\\bar y\\bar x  - \\sum_{i=1}^n y_ix_i  = \\beta_1n\\bar x^2 - \\sum_{i=1}^n \\beta_1x_i^2 \n",
    "\\\\\n",
    "\\tag{2.4}\n",
    "\\beta_1 = \\dfrac{n\\bar y\\bar x  - \\sum_{i=1}^n y_ix_i }{n\\bar x^2 - \\sum_{i=1}^n x_i^2}\n",
    "\\quad\\text{or}\\quad \\dfrac{\\sum_{i=1}^n y_ix_i - n\\bar y\\bar x }{\\sum_{i=1}^n x_i^2-n\\bar x^2}\n",
    "\\end{align}\n",
    "$$\n",
    "and $\\beta_0$ was already found:\n",
    "$$\n",
    "\\beta_0 = \\bar y - \\beta_1\\bar x \n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 128,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The estimated regression coefficients:\n",
      "b_0 = 2.3332266161708524            \n",
      "b_1 = 4.905402532103041\n"
     ]
    }
   ],
   "source": [
    "# Get the number of observervations\n",
    "n = np.size(x)\n",
    "\n",
    "# Calculate the mean of the independent and dependent variables\n",
    "m_x, m_y = np.mean(x), np.mean(y)\n",
    "\n",
    "# Calculate the summed squared deviation and summed squared co-deviation  \n",
    "SS_xy = np.sum(y*x) - n*m_y*m_x \n",
    "SS_xx = np.sum(x*x) - n*m_x*m_x  #equivalent to np.sum((x-m_x)**2)\n",
    "\n",
    "# Calculate the regression coefficients \n",
    "b_1 = SS_xy / SS_xx \n",
    "b_0 = m_y - b_1*m_x\n",
    "\n",
    "# Display the regression coefficients\n",
    "print(\"The estimated regression coefficients:\\nb_0 = {}  \\\n",
    "          \\nb_1 = {}\".format(b_0,b_1)) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Results and Insights - Compare the model to the data\n",
    "Visualize the resulting regression model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 129,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAX4AAAEGCAYAAABiq/5QAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+j8jraAAAgAElEQVR4nO3deXxU1f3/8ddhUVBZZBGXgEGLW4GwJIooCm64VetaVEDASmlF0Z+ltYuK21da6E9FqZQKWFu+oiI/l9riAlJEUAgQFMW9gKEUAQsiNEiS8/tjkgjJ7HPXue/n4+FDkrmZOXdy877nfu6Zc4y1FhERiY5GfjdARES8peAXEYkYBb+ISMQo+EVEIkbBLyISMU38bkA62rVrZwsLC/1uhohIqCxfvnyLtbZ9/e+7FvzGmOnAhcAX1tquNd9rAzwFFAJrgSuttf9J9VyFhYWUlpa61VQRkbxkjFkX7/tulnoeB86t973bgHnW2i7AvJqvRUTEQ64Fv7V2IfBlvW9fDPyp5t9/Ar7v1uuLiEh8Xt/c7WCt3QhQ8/9DEm1ojBlpjCk1xpRu3rzZswaKiOS7wN7ctdZOBaYCFBcXN5hXYs+ePZSXl1NRUeF52yS/NGvWjIKCApo2bep3U0Q84XXwbzLGHGat3WiMOQz4ItsnKi8vp0WLFhQWFmKMcbCJEiXWWrZu3Up5eTmdO3f2uzkinvC61PMCcG3Nv68Fns/2iSoqKmjbtq1CX3JijKFt27a6cpRIcS34jTFPAkuAY40x5caY64DxwNnGmI+Bs2u+zuU1cm+oRJ6OIwmU6mrYtAlcnDnZtVKPtfaqBA+d6dZrioiEWnU1DBgAixdD377w+uvQyPn+uaZsyEHjxo3p0aMHXbt25Xvf+x7btm3zu0l17rjjDl577bWcn2fBggUYY5g2bVrd91auXIkxhokTJ6b9PGvXrqVr165ZbZPo+07to0hgbN4cC/3Kytj/XRrRqODPQfPmzSkrK2P16tW0adOGyZMn5/ycVVVVDrQM7r77bs466yxHnqtbt2489dRTdV/PmjWLoqIiR547F07uo0ggHHJIrKffpEns/4ckHPGeEwW/Q04++WQ2bNgAwKeffsq5555L79696devHx988EHd9/v06UNJSQl33HEHBx10EBDrVQ8YMICrr76abt26UVVVxdixYykpKaF79+784Q9/AGDjxo2cdtppdVcZb7zxBlVVVQwbNoyuXbvSrVs3HnjgAQCGDRvG7NmzAZg3bx49e/akW7dujBgxgt27dwOxqTDuvPNOevXqRbdu3eraWV+nTp2oqKhg06ZNWGuZO3cu5513Xt3jZWVl9OnTh+7du3PJJZfwn//EZuFYvnw5RUVFnHzyyfucFBPtX6b23sdE+7Jz505GjBhBSUkJPXv25Pnnsx5PIOI+Y2LlnfJyWLAg9rULAjuOPxN3vfge7//rK0ef84TDW3Ln976b1rZVVVXMmzeP6667DoCRI0cyZcoUunTpwttvv81PfvIT5s+fz5gxYxgzZgxXXXUVU6ZM2ec5li5dyurVq+ncuTNTp06lVatWLFu2jN27d3PKKadwzjnnMGfOHAYOHMivfvUrqqqq2LVrF2VlZWzYsIHVq1cDNCg3VVRUMGzYMObNm8cxxxzD0KFDefTRR7n55psBaNeuHStWrOD3v/89EydO5LHHHou7j5dffjnPPPMMPXv2pFevXuy///51jw0dOpSHH36Y008/nTvuuIO77rqLBx98kOHDh9d9f+zYsXXbT5s2Le7+5XqTNd6+3HfffZxxxhlMnz6dbdu2ceKJJ3LWWWdx4IEH5vRaIq5p1Ag6dHD3JVx99jz33//+lx49etC2bVu+/PJLzj77bL7++msWL17MFVdcQY8ePfjRj37Exo0bAViyZAlXXHEFAFdfffU+z3XiiSfWjSN/5ZVXeOKJJ+jRowcnnXQSW7du5eOPP6akpIQZM2Ywbtw43n33XVq0aMFRRx3FZ599xo033sjcuXNp2bLlPs/74Ycf0rlzZ4455hgArr32WhYuXFj3+KWXXgpA7969Wbt2bcJ9vfLKK3nmmWd48sknueqqb+/bb9++nW3btnH66afv8/z1vz9kyJC6n0m0f7mKty+vvPIK48ePp0ePHvTv35+KigrWr1+f82uJhFle9PjT7Zk7rbbGv337di688EImT57MsGHDaN26NWVlZRk91949UGstDz/8MAMHDmyw3cKFC3nppZcYMmQIY8eOZejQoaxatYqXX36ZyZMn8/TTTzN9+vR9niuZ2p5748aNqaysTLjdoYceStOmTXn11Vd56KGHWLx4cdLntdYm7MEn2r9kJ550xNsXay3PPvssxx57bE7PLZJP1ON3QKtWrZg0aRITJ06kefPmdO7cmWeeeQaIBc+qVasA6NOnD88++ywQu0GayMCBA3n00UfZs2cPAB999BE7d+5k3bp1HHLIIVx//fVcd911rFixgi1btlBdXc1ll13GPffcw4oVK/Z5ruOOO461a9fyySefAPDnP/+5rheeqbvvvpvf/OY3NG7ceJ99P/jgg3njjTf2ef7WrVvTqlUrFi1aBMDMmTNT7p8bBg4cyMMPP1x3Aly5cqUrryMSJnnR4w+Cnj17UlRUxKxZs5g5cyY//vGPuffee9mzZw+DBg2iqKiIBx98kMGDB/O73/2OCy64gFatWsV9rh/+8IesXbuWXr16Ya2lffv2PPfccyxYsIAJEybQtGlTDjroIJ544gk2bNjA8OHDqa6uBuD+++/f57maNWvGjBkzuOKKK6isrKSkpIRRo0ZltY99+/aN+/0//elPjBo1il27dnHUUUcxY8YMAGbMmMGIESM44IAD9undJ9q/ZD788EMKCgrqvq69iZ3K7bffzs0330z37t2x1lJYWMhf//rXtH5WJF+ZVKWAICguLrb1F2JZs2YNxx9/vE8tys6uXbto3rw5xhhmzZrFk08+qVEmARHG40kkFWPMcmttcf3vq8fvoeXLlzN69GistbRu3XqfWryIiFcU/B7q169fXb1fRMQvurkrIhIxCn4RkYhR8IuIRIyCX0QkYhT8OTDGcOutt9Z9PXHiRMaNG5f0Z6ZMmcITTzyR0+s+/vjjtG/fnp49e9KlSxcGDhyY8pO0AM899xzvv/9+Tq8tIuGn4M/B/vvvz5w5c9iyZUvaPzNq1CiGDh2a82v/4Ac/YOXKlXz88cfcdtttXHrppaxZsybpzyj4JVI8WMkqrBT8OWjSpAkjR46M+ynSdevWceaZZ9K9e3fOPPPMuonBxo0bV7eAyaRJkzjhhBPo3r07gwYNorq6mi5durC5ZvGF6upqvvOd76Q8sQwYMICRI0cydepUAP74xz9SUlJCUVERl112Gbt27WLx4sW88MILjB07lh49evDpp5/G3U4kL9SuZFVQAP37x76WOtEKfhd6ADfccAMzZ85k+/bt+3x/9OjRDB06lHfeeYdrrrmGm266qcHPjh8/npUrV/LOO+8wZcoUGjVqxODBg+vmtXnttdcoKiqiXbt2KdvRq1evujnoL730UpYtW8aqVas4/vjjmTZtGn379uWiiy5iwoQJlJWVcfTRR8fdTiQveLSSVVhFJ/hd6gG0bNmSoUOHMmnSpH2+v2TJkrqpl4cMGVI3WdneunfvzjXXXMNf/vIXmjSJfZZuxIgRdfcApk+fzvDhw9Nqx95Tb6xevZp+/frRrVs3Zs6cyXvvvRf3Z9LdTiR0PFrJKqyiE/wu9gBuvvlmpk2blnSGyXhTFL/00kvccMMNLF++nN69e1NZWUnHjh3p0KED8+fP5+23395npatkVq5cWTfXzLBhw3jkkUd49913ufPOO6moqIj7M+luJxI6Hq1kFVbRCX4XewBt2rThyiuv3KdU0rdv37qpl2fOnMmpp566z89UV1fz+eefM2DAAH7729+ybds2vv76ayA2e+XgwYO58sor95kCOZF//OMfTJ06leuvvx6AHTt2cNhhh7Fnz559pkNu0aIFO3bsqPs60XYieaF2JSuFfgPRmauntgeweXMs9B0+GG699VYeeeSRuq8nTZrEiBEjmDBhAu3bt6+bqrhWVVUVgwcPZvv27VhrueWWW2jdujUAF110EcOHD09a5nnqqadYtGgRu3btonPnzjz77LN1Pf577rmHk046iSOPPJJu3brVhf2gQYO4/vrrmTRpErNnz064nYjkN03LHEClpaXccsstdYubiPvy+XiS6NK0zCExfvx4Hn30UZVeRMQ10anxh8Rtt93GunXrGtwTEBFxSqiDPwxlKgk+HUcSNaEN/mbNmrF161b90UpOrLVs3bqVZs2a+d0UEc+EtsZfUFBAeXl53fQGItlq1qzZPgu5i+S70AZ/06ZN6dy5s9/NEBEJndCWekREJDsKfpEo0VTFgoJfJDrqT1S4caNOAE4LyYnVl+A3xtxijHnPGLPaGPOkMUZDKkTctvdEhW+8AR07aq56J4VoDQDPg98YcwRwE1Bsre0KNAYGed0OkcjZe6JCa6GqSnPVOylEawD4VeppAjQ3xjQBDgD+5VM7RKKjdqLCzz+Hfv00V73TQrQGgOfDOa21G4wxE4H1wH+BV6y1r9TfzhgzEhgJ0KlTJ28bKZKvGjWCQw+NzVHv0ky1keXyDMBO8qPUczBwMdAZOBw40BgzuP521tqp1tpia21x+/btvW6mSH7TXPXuCMn76kep5yzgn9bazdbaPcAcoK8P7RARiSQ/gn890McYc4CJrUd4JrDGh3aIiESS58FvrX0bmA2sAN6tacNUr9shEmghGQ8u4eTLqB5r7Z3W2uOstV2ttUOstbv9aIdIIIVoPLikKWAncn1yVyRoQjQeXNIQwBO5gl8kaEI0HlzSEMATuYJfxC3ZXt7XjgcvL4+Ntw/40EBJIYAncgW/iBtyvbwPyXhwSUMAT+QKfhE3BPDyXnwUsBO5gl/EDQG8vA+dgI2EyScKfhE3BPDyPlQCOBImnyj4RdwSsMv7UFGpzFUKfhEJlurqWHnn5JNVKnOJgl9EgqO2xNOxY+xKaf36SJfK9lS5U+LyfD5+EZGE6pd4GjUKX+hXV+c0J/8r7/2bW59ZxY6KSgAW/LQ/he0OdLSJCn4RCY7a0VCLF4ezxFN7xVLb/tdfj528Uii87aW43y8qaEWnNgc43UoFv4gESIhWsYor3k3pDh0abGatpfMv/pbwaWaPOpniwjauNVPBL/kpx8tt8VHtaCivOXHMJLliqaq2HP3LxGH/3cNb8tJN/bJ73Qwp+CX/ZHm5Hfd5dPJIX5jfL6eOmXpXLDu/qeK7d76ccPMjWjfnzdvOyKHh2VHwS/5J83I7KaeCICrC/n45cczU+GLnN5z4QGnCx08sbMPTo07OtqWOUPBL/nHiBqGDQRAJYX+/cjxmPtq0g3MeWJjw8XYH7Ufpr8/OtZWOUfBL/nHiBmHYR5c4KZ0STtjfryyOmcWfbOHqx95O+HhWPXuPymUKfslPud4gDPvoEqekW8LJh/crjWPm+bINjJlVlvDx4w9ryd/HZHmD1sNymYJfJBG/Rpfsze8bppmUcILwfrlg8uufMOHlDxM+/r2iw3n4qp65v5CH5TIFv0hQBeGGadhLOFn6xZx3eXLp+oSPjx7wHX468FhnX9TD91rBLxJUQbhhmg8lnDRdNfUtlny2NeHj913SlWtOOtK9Bnj4Xiv484nfZQFxVlB623lawgEovvdVtnz9TcLHZwwrYcBxHr7vHr3XCv58EYSygMSX7Qk50x6gTvxpSTQvTq0XRp9C94LWHrXGHwr+fBGEsoA0lOsJOd0eoE78SaUK+4VjB9CprfOToQWVgj9fBKUsIPvy6oSsE38DqcJ+5e1nc/CB+3nUmmBR8OeLCN2Ey5ifJRCvTsg68QOpw/6De86lWdPGHrUmuBT8+SSPb8JlrDbs27WDM87wrwTi1Ql579dp1w6++CIyHYBUYf/p/5xP40b5/z5kQsEv+WfvendJCSxdClVV/pVAvDohN2oE7dtHotafKuzXjr/Ao5aEk4Jf8s/e9e5ly2LhX1oajRJIHtf6fQ37PBsxpeCX/FO/3j1/PmzZkjd/tEllWusPeKAFomefhyOmjLXW7zakVFxcbEtLE89vLS4KeDAkFNZ2OyHdfQ9goKVakhB8KONs2gQFBbGrqCZNoLw8NFdRxpjl1tri+t/3pcdvjGkNPAZ0BSwwwlq7xI+2SBIBDIa0RflGd7r7HpCyUMWeKo67fW7SbXyt2efhiCm/Sj0PAXOttZcbY/YDovPJiTAJSDCIS3wMtC1f76b43teSbrP2Nxd+28P2Ux4OlfY8+I0xLYHTgGEA1tpvgMSTZYh/8rCnA0S7DLS3XAIti/cw1SpVUNOztxb694+FflCOuzy7gvS8xm+M6QFMBd4HioDlwBhr7c56240ERgJ06tSp97p16zxtp9TIt5AMc/kqKDJ4D+d/sIkRjye/Pxe3jJNvx122cnwfEtX4/Qj+YuAt4BRr7dvGmIeAr6y1tyf6Gd3cFceE+EZdYKR4D6cv+id3//X9pE+hcfZpcKCTEqSbu+VAubW2drHK2cBtPrRDoihfy1dOyXJ93eufKOXV9zclfWqFfYZcvMfmefBba/9tjPncGHOstfZD4ExiZR8R9+XhjTrHZLi+bu97XmXrfyshyfBLhX2G9j7xuthJ8WtUz43AzJoRPZ8Bw31qh0RRPt2oc7IWnkYPM9UHqkBhn7V4J16XOim+BL+1tgxoUHcSkQw4faM6QQ8zVdgfd2gL5t58WvavKzGJTrwudFI0ZYNIWDldA96rDFb4QGnSEs7FRYfz0FU9s3+tqMny3olbFPwiYeVwUKTq2Y895xhuuOuH377eDzK4wojy8MwM75148T5prh4JlygHSDw5vh+pwn7qkN6c891DY19kOxQ26p+d8HEIcZCGc4pkJ+oBEk8WN6pThf2Lo0+lW0Grhg9ke4Xhx9QfQeogBHAIsYLfL0E6MMNCcwdlLVXYL31kMIfs/rqmNxon9CH7UoTXwRe0DkIAhxAr+P0QtAMzLALYcwqytNafbdIoNi/O7q/Te0+zGQrrVPCl21kKYgchYEOIFfx+COKBGQYB7DkFTaqw/+f952Pqv29evKe5Bl8mnSV1EFJS8PtBB2b66vfyAtZzCoKcV6kKw3uaSWdJHYSUFPx+0IGZHpXEEgrEkoReyrSzFIaTGfh2r0/B75ewHJh+UkmsTiCXJPRSPnaWfOzYKPgluKJaEqvpBe5s1Ybvjnsl6aauhH1QR5zlW2fJx46Ngl8y51Uw5GMvL4W1m3fQ/3dprFLlFpXXvONjx0bBL5nxOhjyrZcXx+sffMHwx5cl3cazMo7Ka97xsWOj4JfMZBoMQSwbBKBNk1//hAkvf5h0m7VLfgsLFnjbxqiW1/ziU8dGwS+ZySQYglg2SLdNLpwchs9Yyusfbk66zdrxF3z72vcv8P7EFMHyWhSlnKTNGDMamGmt/Y83TWpIk7QFTLqhGMT1bdNpk4MnrONu/zsVe6qTbpPXo3HEV7lM0nYosMwYswKYDrxswzClp7gn3cvTIJYN0mlTjnVu31epCkApS4ItrWmZTewz3ucQWyKxGHgamGat/dTd5sWoxx9iQQyhVG2yNjZ/Te3JIY06u+9hXyuI5TXxTU7TMltrrTHm38C/gUrgYGC2MeZVa+3PnG2q5JUgjspJ1aY069yBCfu9RX1UThA7GgGUMviNMTcB1wJbgMeAsdbaPcaYRsDHgIJf8k+Ck0OqsN+vcSM+uu88t1qVWhDLa17R1U7a0unxtwMutdau2/ub1tpqY8yF7jRLJDhShf05J3Rg6tAGV9P+iPKonKhf7WQgZfBba+9I8tgaZ5sjgZPLpXOIL7tThf0vzz+Okacd7VFrMuRXec3v33eUr3YypHH8klgul84hvOxOFfZ/vu5E+nVp71FrQiYIv+8oX+1kSIutS2K5jMMP4hj+OFKF/Rs/G0DHNgd41JoQC8nvO2q02LpkLpdL5wBfdqcK+zV3n0vz/Rp71Jo8EeDftzSk4JfEcrl0zvWy2+F6cVZLEkr6VGYJFQW/JJfLjcJsf7ayEk47DZYty6leHLlVqvwWxM9sSFwK/lT8HqkQNdXV0K8fvPVW7OsMh+Up7HOk4z0SFPzJBGGkQtRs3hzr6dcqKUlaL478koRO0vEeGQr+ZPSBEO8dcgiccgq8+WYs9BctatDzrNhTxXG3z036NAr7LOTr8a6rmAYU/MlkM1JBB1luEtwk3PRVBSf9z7ykP6qwz1E+jszRVUxcGsefSiZBroPMUWWfb+P7k99Muo3C3mH51nGJ+OcLAjeO3xjTGCgFNlhrgzvnTyYjFfL1UtlDz5dtYMyssqTbKOxdlG8jc/LxKsYBfpZ6xgBrgJY+tsFZOsiy8tu5H/D7BcmXdlDYS1b0+YK4fAl+Y0wBcAFwH/B//GiDK3SQpW3Q1CW89dmXSbdR2EtcmZaj8u0qxgF+9fgfJDaPf4tEGxhjRgIjATp16uRRsxyQ7kGWay01hLXYQC5cIuGi+2iO8Dz4a+bw/8Jau9wY0z/RdtbaqcBUiN3c9ah53sj14A3Rwa+wF0fpPpoj/OjxnwJcZIw5H2gGtDTG/MVaO9iHtvgjk4M3Xs8+4Ae/wl5co/tojvA8+K21vwB+AVDT4/9ppEIf0j94E/XsA3jwpwr7bke04sUbT/WoNRETwrJf1nQfzRH6AFe6nPzjSvfgTdSzD8jBnyrsf3T6UfzivOM9ak0ExDsGQ1T2c4xu1ubM1+C31i4AFvjZhrS48ceVzsGbrGfv08GfKuwf+EERl/Qs8Kg1eax+yCc6BgNe9pNgUo8/HW7+cSW7kghJz372qJMpLmzjUWsiIF7IJzoGA1j2k+BT8KfDrT+udK4kAtqzX/TzARQcrCUJXREv5BMdgwHpHEi4KPjT4dYfV8Au01OF/ft3D+SA/XTIuC5eyCc7BlXzlgzprzhdyf64sr3xG4DLdC1JGECJQl4BLw5R8Ocqlxu/Pl2ma5WqEFDIi4sU/LnKtVzj0R+4wl5Eain4cxWAck08WpJQRBJR8OcqQKMqvqms5phf/z3pNgp7EVHwO8HHeuzWr3fT+97Xkm6jsBeRvSn43ebCPCofbdrBOQ8sTLqNwl5EElHwu8nBqR7mf7CJEY8nX3dYYS8i6VDwuynHET+Pv/lPxr34ftJtFPYBF6WZMyU0FPxuymLEz89mr+Lp0vKk2yjsQyKKM2dKKCj43ZTmiJ+LJ7/Jqs+3JX0qhb0HnO6dp3PFl2iqZV0liIsU/G5LMOKn5L7X2Lxjd9If9SXsoxo6bvTOU13xxXtN0FWCuM5YG/zlbIuLi21pafIbm57JIRhTfXq23UH7U/rrs3JpXW6iXJrYtAkKCmK98yZNoLzcmSG6yY6XeK8J7rRDIskYs9xaW1z/++rxZyKLYEwV9t/vcTgPDurpZCuz58ZsoWG5gnDrE9jJPuOR6DUD+ElwyS/q8WcizV5hqrC/75KuXHPSkW61MnvWQv/+34bOggW5hXXYriD8OEmpxi8uUo/fCUl6hf0nvM7arbsS/uj/+0lfenY62ItWZs/p6ScCtt5ASn58Ajvea2pmTnGZgj8T9YKxMMUkaKFcpcrJ0AnoBHYiUafgz1DhL5NPgvbeXQM5cH+9rUCgJrATkW8podKQqmb/8X3n0bRxgGvXflLZQiRwFPwJ5LQkoW7OiUiAKfj34sgqVWEbyRJFOjFLxEU++B1fkjBsI1miRidmkWgGv6vrzwZpJIt6tg1lO3+O0/S7ER9FIvittazZuIPHFn3GnBUbGjzeo2NrnrvhFGdeLCgjWdSzjS+b+XOcft/0uxGf5fUnd9//11e8+M6/+Pu7Gxt8uOrsEzrwx6ENPtCWP9yaeyYfZDp/jtPvm3434pFIfnL3yaXr+d+l6+l7dFt+dPrRnH1CB9odtL/fzfJGkEpOQZPN/DlO0u9GfJbXPf5NX1WwX+NGHHzgfi60KgRUR86OavySJyLZ4+/QspnfTfCXPjyVHS/eN/1uxEfRu6NUXR2rsYbgSkdExA3RCv7a0RQFBbHph6ur/W7Rt5w+IekEJyIJeB78xpiOxpjXjTFrjDHvGWPGuPZi9cMv3hjuIHD6hBTkE5yI+M6PHn8lcKu19nigD3CDMeYEx18lXvjVjqZo0iRYoymcPiEF9QQnIoHgefBbazdaa1fU/HsHsAY4wvEXihd+tR+uKi/PfXWpTCUrvTh9QgrqCU5EAsHXGr8xphDoCbwd57GRxphSY0zp5mx6rInCr3Y0hdehn6z04vQJyasTnO4jiISSb+P4jTEHAf8A7rPWzkm2bdZr7gZlrHQ+flJT0w6IBF6icfy+/KUaY5oCzwIzU4V+Trzu3SfqAedj6UX3EURCy49RPQaYBqyx1v5fr1/fNcnKOX7eW3BL/ZOZtSr5iISEHz3+U4AhwBnGmLKa/8737NXdqkun6gH7cW/BTbUns/XrY+9lx44aOioSEn6M6llkrTXW2u7W2h41//3Nkxd3c3x7PpZzUmnUKPbfkiUq+YiESLTuxrlZlw5zOSeXq6AonvBEQi5awe92SIWxnJPrVVCYT3giEZXXs3M2EJTVsYLEiTWCNdOkSKhEq8cP4eyVu0mlGpHIiVaPXxrSVZBI5Cj4RaUakYiJXqlHRCTiFPwiIhGj4K+lmSZFJCIU/KAVq0QkUhT8oJkmRSRSohf88Uo6GssuIhESreBPVNLRtAMiEiHRCv5kJR0vPtGrG8giEgDRCn4/Szq6gSwiARGtT+76OT2BE5OhiYg4IFo9fvBvkrZ27aCkRDeQRcR30Qt+P1RXwxlnwNKlUFwM8+frBrKI+EbB74XaMk9VFZSWwpYtfrdIRCJMwe8FfU5ARAIkWjd3/aI570UkQBT8XtGc9yISECr1BJk+8CUiLlDwB5U+8CUiLlHwB5VmDBURlyj4g0ojgUTEJbq5G1QaCSQiLlHwB5lGAomIC1TqERGJGAW/iEjEKPidpHH3IhICCn6naNy9iISEL8FvjDnXGPOhMeYTY8xtfrTBcRp3LyIh4XnwG2MaA5OB84ATgKuMMSd43ZO4I/QAAAUISURBVA7Hady9iISEH8M5TwQ+sdZ+BmCMmQVcDLzvQ1uco3H3IhISfpR6jgA+3+vr8prv7cMYM9IYU2qMKd0clrKJX8s6iohkwI/gj5eKDYbBWGunWmuLrbXF7du396BZIiLR4EfwlwMd9/q6APiXD+0QEYkkP4J/GdDFGNPZGLMfMAh4wYd2iIhEkuc3d621lcaY0cDLQGNgurX2Pa/bISISVb5M0mat/RvwNz9eW0Qk6vTJXRGRiDE2BPPKGGM2A+uy+NF2wBaHmxMGUdzvKO4zaL+jJtP9PtJa22BYZCiCP1vGmFJrbbHf7fBaFPc7ivsM2m+/2+E1p/ZbpR4RkYhR8IuIREy+B/9UvxvgkyjudxT3GbTfUePIfud1jV9ERBrK9x6/iIjUo+AXEYmY0Ad/qtW8TMykmsffMcb08qOdTktjv6+p2d93jDGLjTFFfrTTaemu3maMKTHGVBljLveyfW5JZ7+NMf2NMWXGmPeMMf/wuo1uSOM4b2WMedEYs6pmv4f70U4nGWOmG2O+MMasTvB47plmrQ3tf8Tm+vkUOArYD1gFnFBvm/OBvxObDroP8Lbf7fZov/sCB9f8+7yo7Pde280nNi3I5X6326Pfd2tiixl1qvn6EL/b7dF+/xL4Tc2/2wNfAvv53fYc9/s0oBewOsHjOWda2Hv8dat5WWu/AWpX89rbxcATNuYtoLUx5jCvG+qwlPttrV1srf1PzZdvEZv+OuzS+X0D3Ag8C3zhZeNclM5+Xw3MsdauB7DW5sO+p7PfFmhhjDHAQcSCv9LbZjrLWruQ2H4kknOmhT3401nNK60Vv0Im0326jlgPIexS7rcx5gjgEmCKh+1yWzq/72OAg40xC4wxy40xQz1rnXvS2e9HgOOJrenxLjDGWlvtTfN8k3Om+TI7p4PSWc0rrRW/QibtfTLGDCAW/Ke62iJvpLPfDwI/t9ZWmfxZAjOd/W4C9AbOBJoDS4wxb1lrP3K7cS5KZ78HAmXAGcDRwKvGmDestV+53Tgf5ZxpYQ/+dFbzyscVv9LaJ2NMd+Ax4Dxr7VaP2uamdPa7GJhVE/rtgPONMZXW2ue8aaIr0j3Ot1hrdwI7jTELgSIgzMGfzn4PB8bbWPH7E2PMP4HjgKXeNNEXOWda2Es96azm9QIwtOZOeB9gu7V2o9cNdVjK/TbGdALmAENC3uvbW8r9ttZ2ttYWWmsLgdnAT0Ie+pDecf480M8Y08QYcwBwErDG43Y6LZ39Xk/sKgdjTAfgWOAzT1vpvZwzLdQ9fptgNS9jzKiax6cQG9lxPvAJsItYDyHU0tzvO4C2wO9rer+VNuSzGaa533knnf221q4xxswF3gGqgcestXGHA4ZFmr/ve4DHjTHvEiuB/NxaG+rpmo0xTwL9gXbGmHLgTqApOJdpmrJBRCRiwl7qERGRDCn4RUQiRsEvIhIxCn4RkYhR8IuIRIyCX0QkYhT8IiIRo+AXyULNfP/vGGOaGWMOrJkLvqvf7RJJhz7AJZIlY8y9QDNik6KVW2vv97lJImlR8ItkqWb+mGVABdDXWlvlc5NE0qJSj0j22hBb/KMFsZ6/SCioxy+SJWPMC8RWheoMHGatHe1zk0TSEurZOUX8UrPCVaW19n+NMY2BxcaYM6y18/1um0gq6vGLiESMavwiIhGj4BcRiRgFv4hIxCj4RUQiRsEvIhIxCn4RkYhR8IuIRMz/B3/RMd64AYyAAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Plot the data again\n",
    "plt.scatter(x,y,s=5,c='r', label = \"Noisy Data\")\n",
    "  \n",
    "# Generate the predicted response \n",
    "y_pred = b_0 + b_1*x \n",
    "  \n",
    "# Plot the regression line \n",
    "plt.plot(x, y_pred, label = \"Regression Model Line\") \n",
    "plt.legend(loc='upper left')\n",
    "\n",
    "# Add labels \n",
    "plt.xlabel('x') \n",
    "plt.ylabel('y') \n",
    "  \n",
    "# function to show plot \n",
    "plt.show() "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "1. https://realpython.com/linear-regression-in-python/\n",
    "- https://towardsdatascience.com/linear-regression-using-python-b136c91bf0a2\n",
    "- https://towardsdatascience.com/a-beginners-guide-to-linear-regression-in-python-with-scikit-learn-83a8f7ae2b4f\n",
    "- https://www.geeksforgeeks.org/linear-regression-python-implementation/\n",
    "- https://www.colorado.edu/amath/sites/default/files/attached-files/ch12_0.pdf\n",
    "- https://towardsdatascience.com/from-linear-to-logistic-regression-explained-step-by-step-11d7f0a9c29"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
