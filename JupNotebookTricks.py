#!/usr/bin/env python
# coding: utf-8

# # Usage Notes - Jupyter Notebook Markdown Tips & Tricks
# ___

# ## General markdown
# 
# ### Headings
# - <code># </code> for titles (h1). Note the space after the `#`
# - <code>## </code> for major heading (h2)
# - <code>### </code> for subheadings (h3)
# - <code>#### </code> for 4th level subheadings (h4)
# - <code>##### </code> for 5th level subheadings (h5)
# 
# ### Emphasis
# 
# - `__text__` or `**text**` for **bold**
# - `_text_` or `*text*` for *italic*
# - `***text***` or `___text___` for ___bold italic___
# - `~~text~~`  for ~~strikethrough~~
# 
# ### Bullets and List
# - <code>- </code> for lists. Note the space after the `-`
# - <code>    - </code> for sublists
# 
# 
# 1. <code>1. </code> for ordered lists
# 3. <code>2. </code> or <code>1. </code> or <code>-. </code> for subsequent ordered items. I.e. the value before the `.` doesn't matter.
# 
# 
# - [x] `- [x]` for completed tasks
# - [ ] `- [ ]` for incomplete tasks
# 
# ### Line Breaks
# - `<br>` to force a line break
# 
# <br>
# 
# - `---`, `___`, `***` or `<hr>` for a horizontal line
# ___
# 
# ### Links
# 
# - `[Section title](#section-title)` for internal links, e.g. `[Links](#Links)` [Links](#Links)
# - `[Link title](URL)` for internal links, e.g. `[Google](https://www.google.com/)` [Google](https://www.google.com/)
# 
# 
# ### Text Formatting
# 
# - `$...$` or `$$...$$` for (centred) math. e.g. `$\dfrac{a + b}{a} = \dfrac{a}{b}$` is $\dfrac{a + b}{a} = \dfrac{a}{b}$
# - <code>\`...\`</code> or `<code>...</code>` for monospaced font. e.g. <code>\`print "Hello World"\`</code> is <code>print "Hello World"</code> 
# - <code>\`\`\`language...\`\`\`</code> for language specific code e.g. 
# 
# <code>\`\`\`python def f(x):
#     """a docstring"""
#     return x**2
#     \`\`\`</code>
# 
# for
# ```python
# def f(x):
#     """a docstring"""
#     return x**2
# ```
# - <code>    </code> for indenting. i.e. 4 spaces or 1 tab monospaced text blocks
# - <code>> </code> for quotes e.g. <code>>One small step function for a computer, one giant step function for computer kind.    </code>
# 
# >One small step function for a computer, one giant step function for computer kind.
# 
# ### Tables
# - `| Column 1 header | Column 2 header |etc|`
# - `| :-------------- | :-------------: | - |`
# - `| left aligned    | centred         | right aligned |`
# 
# | Column 1 header | Column 2 header |etc|
# | :- | :-: | -|
# | left aligned | centred | right aligned|
# 
# ### Images and Graphis
# - Local image `![Alt text](path/to/image.type)` e.g. `![image](./PlaceholderImage.png)` ![image](./PlaceholderImage.png)
# - Remote image `![Alt text](URL)` e.g. `![image](https://via.placeholder.com/100x30)` ![image](https://via.placeholder.com/100x30)
# - `&#0000;` for HTML UTF-8 Geometric shapes e.g.`&#9658;` for &#9658;

# ## Tricks
# 
# <details>
#     <summary><h3 style="display:inline-block;"> Collapsible Markdown!!! </h3></summary>
# 
# ```html
# <details>
#     <summary><h3 style="display:inline-block;"> Collapsible Markdown!!! </h3></summary>
# Markdown here
# </details>
# ```
#     
# #### yes, even hidden code blocks!
# 
# ```python
# print("hello world!")
# ```
# </details>
# 
# 
# ### Unrendered Markdown Content (For Commenting)
# 
# - `[Unused link id]:# (This is an unrendered comment!)`
# - uses link ids and an empty URL (the #)
# 
# [.]:# (This is an unrendered comment!)
# 
# ### Text Columns
# ```html
# <div class="row">
#     <div  class="col-md-6">
#         Markdown
#     </div>    
#     <div >
#         Markdown
#     </div>    
# </div>
# ```
# <div class="row">
#   <div  class="col-md-6">
#       <br/>
#     Lorem ipsum dolor sit amet, consectetur adipiscing elit.    Maecenas quis nunc pulvinar urna faucibus tincidunt ut vestibulum ligula. Sed placerat sollicitudin erat, quis dapibus nibh tempor non. 
#       <br/>
# 
# ```python
# print("hello world!")
# ```
# 
#       Test
#           Test
#               Test
#                   Test
#                       Test
#                           Test
#                               Test
#       
# Id | Syntax      | Description 
# --|:---------:|:-----------:
# 1|Header      | Something  here
# 2|More here   | Text
#     
#   </div>
#     
#   <div >
#       <br/>
# Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis nunc pulvinar urna faucibus tincidunt ut vestibulum ligula. Sed placerat sollicitudin erat, quis dapibus nibh tempor non. 
#   <br/>
#     
#   $$
#   \begin{align}
#   {x} & = \sigma(y-x) \tag{3-1}\\
#   {y} & = \rho x - y - xz \tag{3-2}\\
#   {x+y+z} & = -\beta z + xy \tag{3-3}
#   \end{align}
#   $$
#     <br/>
#   </div>
# </div>

# ## MathJax Latex Equations
# 
# ### Numbered Equations
# - `$Equation \tag{1.0}$` e.g. `$ y = mx + b \tag{1.0}$` is 
# $ y = mx + b \tag{1.0}$
# 
# ### Strikethrough
# 
# - `$\require{cancel} \cancel{test}$` $\require{cancel} \cancel{test}$
# - `$\require{enclose} \enclose{horizontalstrike}{test}$` $\require{enclose} \enclose{horizontalstrike}{test}$

# ## HTML Styling
# ### Inline HTML
# ```html
# <p style = "color: red;"> Test </p>
# ```
# <p style = "color: red;"> Test </p>
# 
# ### Hiding `In [#]:` and `Out [#]:`
# 
# ```html
# <style>
# .prompt{
#     width: 0px;
#     min-width: 0px;
#     visibility: collapse;
# }  
# .rendered_html pre, .rendered_html table{
#     font-size:100%;
#     line-height: 125%;
# }
# .CodeMirror, .CodeMirror pre, .CodeMirror-dialog, .CodeMirror-dialog .CodeMirror-search-field, .terminal-app .terminal {
#     font-size: 100%;
#     line-height: 125%;
# }
# </style>
# ```

# In[38]:


get_ipython().run_cell_magic('html', '', '<style>\n.prompt{\n    width: 0px;\n    min-width: 0px;\n    visibility: collapse;\n    visibility: visible;\n}  \n\n.rendered_html pre, .rendered_html table{\n    font-size:100%;\n    line-height: 125%;\n}\n\n.CodeMirror, .CodeMirror pre, .CodeMirror-dialog, .CodeMirror-dialog .CodeMirror-search-field, .terminal-app .terminal {\n    font-size: 100%;\n    line-height: 125%;\n}\n</style>')


# ### Changing Markdown Font

# In[105]:


get_ipython().run_cell_magic('html', '', '<style>\ndiv.text_cell_render {\n    #font-family: sans-serif;\n    #font-family: serif;\n    font-family: Georgia, Cambria, "Times New Roman", Times, serif;\n}\n\ndiv.text_cell_render p {\n   font-family: sans-serif;\n}')


# ### Loading Custom CSS

# In[104]:


from IPython.display import HTML
HTML('<style>{}</style>'.format(open('custom.css').read()))


# ## Hiding Code When Printing...

# In[30]:


get_ipython().run_cell_magic('html', '', '<style>\n    @media asdfsdf {\n\n    div.input {\n      display: none;\n      padding: 0;\n    }\n\n    div.output_prompt {\n      display: none;\n      padding: 0;\n    }\n\n    div.text_cell_render {\n      padding: 1pt;\n    }\n\n    div#notebook p,\n    div#notebook,\n    div#notebook li,\n    p {\n      font-size: 11pt;\n      line-height: 135%;\n      margin: 0;\n    }\n\n    .rendered_html h1,\n    .rendered_html h1:first-child {\n      font-size: 16pt;\n      margin: 7pt 0;\n    }\n\n    .rendered_html h2,\n    .rendered_html h2:first-child {\n      font-size: 14pt;\n      margin: 6pt 0;\n    }\n\n    .rendered_html h3,\n    .rendered_html h3:first-child {\n      font-size: 13pt;\n      margin: 6pt 0;\n    }\n\n    div.output_subarea {\n      padding: 0;\n    }\n    }\n\n    @page {\n      size: A4;\n    }\n    \n</style>')


# ## References
# 
# - https://www.ibm.com/support/knowledgecenter/en/SSGNPV_2.0.0/dsx/markd-jupyter.html
# - https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/Working%20With%20Markdown%20Cells.html
# - https://dotcms.com/docs/latest/markdown-syntax
# - https://medium.com/analytics-vidhya/the-ultimate-markdown-guide-for-jupyter-notebook-d5e5abf728fd
