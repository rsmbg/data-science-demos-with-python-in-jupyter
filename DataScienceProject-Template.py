#!/usr/bin/env python
# coding: utf-8

# # Data Science Project - Template
# <hr>

# ## 1. Project Purpose - Define the purpose and describe the background motivation
# To demonstrate ...

# ## 2. Data Source - Generate or load the data for this project
# For this project, data will be...
# 

# ## 3. Data Structure and Quality - Explore and clean the data
# The data of this project...
# 

# ## 4. Data Model - Describe, justify and apply the model(s) chosen for the project
# This project will use a model...
# 

# ## 5. Results and Insights - Show performance results, conclusions and reccommendations
# The chosen model generated...

# ## References
# ...
