# Data Science Demos with Python in Jupyter

A repository to create short projects documenting and demonstrating various python functionalities, specifically for data science, in Jupyter notebooks.